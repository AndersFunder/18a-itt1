---
Week: 49
Content: Python and networking
Material: See links in weekly plan
Initials: MON
---

# Week 49 Python and networking

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* You have a gitlab account

### Learning goals
* Working with code
  * Level 1: Know how to get and run code
  * Level 2: Able to code simple python program and modify existing code
  * Level 3: Able to create and colaborate on simple python programs and share them

* Working with network packages
  * Level 1: Know what client/server architecture is
  * Level 2: Able to setup a client/server system
  * Level 3: Able to setup a client/server system and read simple TCP networking traffic 

## Deliverable
* Power point - 5 slides no text
    
    For (any) presentation, pease consider "who is the audience?", "what should they get out of it?"

## Hands-on time

* Exercise 1 (LG lvl 1/2)

  Go [here](https://gitlab.com/EAL-ITT/ex-python-network/tree/master).
  
  Use git to download it to your local machines.
  
  Presentations on class: 5 slides no text.
  * show outcome using screenshots
  * any surprises?
  * 1-2 questions for the class

* Exercise 2 (LG lvl 2)

  1. Use wireshark to collect and see the data transmitted. 
  
     Wireshark [display filters](https://wiki.wireshark.org/DisplayFilters) are handy fir this.
  
  2. Modify the program to send specific text strings.
  
  3. Refind the information from your program in wireshark. Notice information your program did not include.
  
  Presentation on class: 5 slides no text
  * show outcome using screenshots
  * any surprises?
  * 1-2 questions for the class
  

* Exercise 3

  We will talk to webservers using HTTP

  0. Get introduced to [HTTP](https://www.cloudflare.com/learning/ddos/glossary/hypertext-transfer-protocol-http/)

  1. Use `curl www.eal.dk -v`. What happened and what does wireshark say?

      You may need to install curl

  2. use telnet to talk to a webserver instead of curl

  3. You already  have an example of how to talk to a server. Change to send the correct text.
  
  Handin on gitlab. Send repo link to MON


## Comments
* We will start with the basics on class: OSI model, client/server model, information and information flow and something about git and gitlab
* I always recommend windows user to install [gitforwindows](https://gitforwindows.org/). That way you have most of the linux command line tools (incl. git) on your windows machine.
* Something about [TCP three-way handshake](https://study-ccna.com/tcp-three-way-handshake/).
